# Variables and Roles in Ansible #

Recently I wanted to test the precedence of the variables settings in [Ansible.](https://www.ansible.com)  
More precisely in [Ansible Roles](http://docs.ansible.com/ansible/latest/playbooks_reuse_roles.html)

**There are three places for the variables definition**

* roles/<rolename>/defaults/main.yml file
* roles/<rolename>/vars/main.yml file
* vars section in the task

But what has precedence? Let's make a simple test:

**Task:**

We need ansible role with name **create_file** which will create two files.  
Name of the first one will be saved in the **/roles/create_file/vars/main.yml** file  
and name of the second one will be in the task **vars section**.

**Solution:**

**.../roles/create_file/tasks/main.yml**

	- name: This is play1 example
  		copy: content="hello world\n" dest={{playbook_dir}}/{{file_name}}

  	    tags:
    		- play1

	- name: This is play2 example
  		copy: content="Play2 content" dest={{playbook_dir}}/{{file_name}}

  		vars:
    		file_name: play2.txt

  		tags:
    		- play2


**.../roles/create_file/vars/main.yml**

	file_name: play_vars_default.txt

**.../roles/create_file/defaults/main.yml**

	file_name: play_default.txt

# Testing the playbook ##

To launch the create_file role let's create simple startPlaybook.yml:

	- hosts: localhost
  
  	  roles: 
    	- create_file

and start it:

	ansible-playbook startPlaybook.yml

**output:**

	[tomask79:ansible-roles-variables tomask79$ ansible-playbook startPlaybook.yml 
 	[WARNING]: Unable to parse /etc/ansible/hosts as an inventory source

 	[WARNING]: No inventory was parsed, only implicit localhost is available

 	[WARNING]: Could not match supplied host pattern, ignoring: all

 	[WARNING]: provided hosts list is empty, only localhost is available


	PLAY [localhost] ********************************************************************************************************************************************************************

	TASK [Gathering Facts] **************************************************************************************************************************************************************
	ok: [localhost]

	TASK [create_file : This is play1 example] ******************************************************************************************************************************************
	changed: [localhost]

	TASK [create_file : This is play2 example] ******************************************************************************************************************************************
	changed: [localhost]

	PLAY RECAP **************************************************************************************************************************************************************************
	localhost                  : ok=3    changed=2    unreachable=0    failed=0   

	tomask79:ansible-roles-variables tomask79$ ls -l *.txt
	-rw-r--r--  1 tomask79  staff  13  5 �no 20:29 play2.txt
	-rw-r--r--  1 tomask79  staff  12  5 �no 20:29 play_vars_default.txt
	tomask79:ansible-roles-variables tomask79$ 

as you can see precedence of the variables settings in Ansible Roles is:

(from the top)

1) vars section in task  
2) vars directory in role  
3) defaults directory  

just for confirmation, delete the main.yml file in the role vars directory. 
And the output file of the second task is going to be play_default.txt

regards

Tomas


